import React, { Component } from 'react';
// import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

// Data
import { Data } from './data';
import _ from 'lodash';

import './styles/App.css';

import HomeContainer from "./containers/HomeContainer/index";
import FavoritesContainer from "./containers/FavoritesContainer/index";

class App extends Component {
    state = {
		animations: Data.Animations,
	    favorites: [1,2]
	};

	toggleFavorite = (favoriteId) => {
		const { favorites } = this.state;

		const _favorites = _.includes(favorites, favoriteId) ? _.without(favorites, favoriteId) : [...favorites, favoriteId];

		this.setState({ favorites: _favorites });
	};

    render() {
    	const { animations, favorites } = this.state;

        return (
			<div className="App">
				<Switch>
					<Route exact path='/' render={() => (
						<HomeContainer animations={animations} favorites={favorites} toggleFavorite={this.toggleFavorite} />
					)}>
					</Route>
					<Route exact path='/favorites' render={() => (
						<FavoritesContainer animations={animations} favorites={favorites} toggleFavorite={this.toggleFavorite} />
					)}/>
				</Switch>
			</div>
        );
    }
}

export default App;