import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import './styles.css';

import GridItem from '../GridItem/index';

const propTypes = {
	items: PropTypes.any,
	toggleFavorite: PropTypes.func
};


const Grid = ({ items, toggleFavorite }) => {


	return (
		<ul className="grid">
			{_.map(items, (item) => <GridItem key={item.name} item={item} className="grid__item" toggleFavorite={toggleFavorite}/>)
			}
		</ul>
	);
};

Grid.propTypes = propTypes;
export default Grid;