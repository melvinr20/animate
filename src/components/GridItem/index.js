import React from 'react';
import PropTypes from 'prop-types';

import './styles.css';

const propTypes = {
	item: PropTypes.shape({
		id: PropTypes.number.isRequired,
		name: PropTypes.string.isRequired,
		link: PropTypes.string.isRequired,
		category: PropTypes.any.isRequired,
		categorylink: PropTypes.any.isRequired,
		image: PropTypes.string
	}),
	toggleFavorite: PropTypes.func
};

const defaultProps = {
	image: 'http:'
};

const GridItem = ({ item, toggleFavorite }) => {
	const clickHandle = () => {
		toggleFavorite(item.id);
	};
	return (
		<li className={`animation_grid__list_item ${item.isFavorite && 'animation_grid__list_item--favorite'}`}>
			<a href="" className="animation_grid__list_item__link">
				{/*<img src={require(`../../../public/images/${item.image}`)} alt={`${item.categorylink} animation`} className="animation_image"/>*/}
			</a>
			<div className="favorite_button" onClick={clickHandle}>
				<i className="fas fa-star favorite_icon"></i>
			</div>

			<div className="animation_grid__list_item__overlay">
				<div className="animation__name_wrapper">
					<h2 className="gif_title">{ item.name }</h2>
				</div>
				<div className={`category__label category__label--${item.categorylink || 'popups'}`}>
					<a href="" className="category__label__text">{ item.category }</a>
				</div>
			</div>
		</li>
	);
};

GridItem.propTypes = propTypes;
GridItem.defaultProps = defaultProps;
export default GridItem;