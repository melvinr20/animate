import React from 'react';

const Header = () => {
    return (
        <header className="header">
            <h1 className="main_title">
                <a href="">aniMate</a>
            </h1>
        </header>
    );
};

export default Header;
