import React from 'react';
import './styles.css';
import NavigationItem from '../MenuItem/index';

const Navigation = () => {

	return (
		<nav className="main_nav">
			<ul className="main_nav__list">
				<NavigationItem />
			</ul>
		</nav>
	);
};

export default Navigation;
