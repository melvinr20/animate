import React from 'react';
import Categories from '../../data/categories-data';


const NavigationItem = () => {

	return (
		Object
			.keys(Categories)
			.map(key => <li key={key} className="main_nav__list__item"><a href={ `#${Categories[key].name}` } >{ Categories[key].name } </a></li>)

	);
};

export default NavigationItem;
