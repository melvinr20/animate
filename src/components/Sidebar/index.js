import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Header from '../Header/index';
import Navigation from '../Menu/index';


class Sidebar extends Component {


	render() {
		return (
			<section className="sidebar">




				<Link to="/">Home</Link>
				<Link to="/favorites">Favorites</Link>

				<Header/>
				<Navigation/>
			</section>
		);
	}
}

export default Sidebar;
