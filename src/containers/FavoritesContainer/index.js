import React, { Component } from 'react';

import PropTypes from 'prop-types';

//Components
import Favorites from '../../screens/Favorites/index';

//Proptypes
const propTypes = {
	animations: PropTypes.array.isRequired

};

const defaultProps = {
	animations: {},
	favorites: []
};

class FavoritesContainer extends Component {
	render() {
		return (
			<div>
				<Favorites {...this.props}/>
			</div>
		);
	}
}

FavoritesContainer.propTypes = propTypes;
FavoritesContainer.defaultProps = defaultProps;
export default FavoritesContainer;