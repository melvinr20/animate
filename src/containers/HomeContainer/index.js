import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Components
import Home from '../../screens/Home/index';

//Proptypes
const propTypes = {
	animations: PropTypes.array.isRequired
	// favorites: PropTypes.array.isRequired
};

const defaultProps = {
	animations: {},
	favorites: []
};



class HomeContainer extends Component {
	render() {
		return (
			<Home {...this.props}/>
		);
	}
}


HomeContainer.propTypes = propTypes;
HomeContainer.defaultProps = defaultProps;
export default HomeContainer;