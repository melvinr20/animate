import Categories from './categories-data';


const availableCategories = Object
	.keys(Categories)
	.map(key => { return [Categories[key].name, Categories[key].link]});

const Animations = [
	{
		id: 1,
		name: 'Ripple Effect',
		link: 'ripple-effect',
		category: availableCategories[0][0],
		categorylink: availableCategories[0][1],
		image: 'ripple.gif'
	},
	{
		id: 2,
		name: 'Popup - Fly In',
		link: 'popup-fly-in',
		category: availableCategories[1][0],
		categorylink: availableCategories[1][1],
		image: 'popup.gif'
	},
	{
		id: 3,
		name: 'Menu Growing',
		link: 'menu-growing',
		category: availableCategories[2][0],
		categorylink: availableCategories[2][1],
		image: 'menu.gif'
	},
	{
		id: 4,
		name: 'Plus Icon',
		link: 'plus-icon',
		category: availableCategories[3][0],
		categorylink: availableCategories[3][1],
		image: 'plusicon.gif'
	},
	{
		id: 5,
		name: 'Hamburger X',
		link: 'hamburger-x',
		category: availableCategories[3][0],
		categorylink: availableCategories[3][1],
		image: 'hamburger.gif'
	}
];

export default Animations;