const Categories = {
	popups: {
		id: 1,
		name: 'Popups',
		link: 'popups'
	},
	buttons: {
		id: 2,
		name: 'Buttons',
		link: 'buttons'
	},
	navigation: {
		id: 3,
		name: 'Navigation',
		link: 'navigation'
	},
	icons: {
		id: 4,
		name: 'Icons',
		link: 'icons'
	},
};

export default Categories;
