import Animations from './animation-data';
import Categories from './categories-data';

const Data = Object.assign({}, { Animations: Animations}, { Categories: Categories});

export { Data };