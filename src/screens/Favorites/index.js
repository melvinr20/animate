import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Sidebar from '../../components/Sidebar/index';
import Grid from '../../components/Grid/index';


const propTypes = {
	animations: PropTypes.array.isRequired,
	favorites: PropTypes.array.isRequired,
	toggleFavorite: PropTypes.func
};

const Favorites = ({ animations, favorites, toggleFavorite }) => {
	const items = _.map(animations, (animation) => {return {...animation, isFavorite: _.includes(favorites, animation.id)}});
	const favoriteAnimations = _.filter(items,'isFavorite');

	return (
		<div className="Favorites">
			<Sidebar />
			<main>
				<Grid items={favoriteAnimations} toggleFavorite={toggleFavorite}/>
			</main>
		</div>
	);
};

Favorites.propTypes = propTypes;
export default Favorites;