import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Sidebar from '../../components/Sidebar/index';
import Grid from '../../components/Grid/index';


const propTypes = {
	animations: PropTypes.array.isRequired,
	favorites: PropTypes.array.isRequired,
	toggleFavorite: PropTypes.func

};

const Home = ({animations, favorites, toggleFavorite}) => {
	// Add isFavourite attributes to animations in case that the id is part of the fav array
	const items = _.map(animations, (animation) => {return {...animation, isFavorite: _.includes(favorites, animation.id)}});

	return (
		<div className="Home">
			<Sidebar />
			<main>
				<Grid items={items} toggleFavorite={toggleFavorite}/>
			</main>
		</div>
	);
};

Home.propTypes = propTypes;
export default Home;